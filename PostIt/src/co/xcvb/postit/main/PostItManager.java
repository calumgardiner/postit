package co.xcvb.postit.main;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import co.xcvb.postit.ui.PostItFrame;
import co.xcvb.postit.ui.factory.PostItFactory;
import co.xcvb.postit.ui.reader.PostItReader;

public class PostItManager implements ActionListener {

	public static void main(String... args) {
		new PostItManager();
	}
	
	private static final String fileName = ".postitsave";
	private List<PostItFrame> postIts = new ArrayList<PostItFrame>();
	
	public PostItManager() {
		populatePostItBoard();
		saveOnClose();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getActionCommand().equals(PostItFrame.DELETE)) {
			postIts.remove(e.getSource());
			((PostItFrame)e.getSource()).setVisible(false);
			((PostItFrame)e.getSource()).dispose();
		} else if (e.getActionCommand().equals(PostItFrame.NEW)) {
			PostItFrame postIt = PostItFactory.getInstance();
			postIt.setListener(this);
			postIts.add(postIt);
			postIt.setVisible(true);
		}
	}
	
	private void populatePostItBoard() {
		PostItReader reader = new PostItReader(fileName);
		try {
			postIts.addAll(reader.readPostIts());
		} catch (IOException e) {
			e.printStackTrace();
		}
		for (PostItFrame postIt : postIts) {
			postIt.setVisible(true);
			postIt.setListener(this);
		}
	}
	
	private void saveOnClose() {
		Runtime.getRuntime().addShutdownHook(new Thread(new RunnableWriter(postIts, fileName)));
	}
	
	
	
}
