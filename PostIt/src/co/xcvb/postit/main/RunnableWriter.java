package co.xcvb.postit.main;

import java.io.File;
import java.io.IOException;
import java.util.List;

import co.xcvb.postit.ui.PostItFrame;
import co.xcvb.postit.ui.writer.PostItWriter;

public class RunnableWriter implements Runnable {

	private List<PostItFrame> postIts;
	private String fileName;

	public RunnableWriter(List<PostItFrame> postIts, String fileName) {
		this.postIts = postIts;
		this.fileName = fileName;
	}

	@Override
	public void run() {
		File file = new File(fileName);
		if (!file.exists()) {
			try {
				file.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		PostItWriter writer = new PostItWriter(fileName);
		try {
			writer.writePostIts(postIts);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
