/**
 * 
 */
package co.xcvb.postit.data;


/**
 * <p>
 * Post it data representation.
 * </p>
 * 
 * @author Calum Gardiner
 * @since 17/10/2014
 * @see String
 * 
 */
public class PostItData implements PostIt {
	
	private String text;

	/**
	 * Empty constructor, creates postit with blank contents.
	 */
	public PostItData() {
		this("");
	}

	/**
	 * Default constructor, defines original text held in post it.
	 * 
	 * @param text
	 */
	public PostItData(String text) {
		this.text = text;
	}

	/*
	 * (non-Javadoc)
	 * @see co.xcvb.postit.data.PostIt#setText(java.lang.String)
	 */
	public void setText(String text) {
		this.text = text;
	}

	/*
	 * (non-Javadoc)
	 * @see co.xcvb.postit.data.PostIt#getText()
	 */
	public String getText() {
		return this.text;
	}
}
