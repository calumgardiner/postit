package co.xcvb.postit.data;

/**
 * <p>
 * POJO representation of a post it.
 * </p>
 * 
 * @author Calum Gardiner
 * @since 17/10/2014
 *
 */
public interface PostIt {

	/**
	 * <p>
	 * Set the text of the post it data.
	 * </p>
	 * @param text
	 */
	public void setText(String text);

	/**
	 * <p>
	 * Get the text held in this post it.
	 * </p>
	 * 
	 * @return <tt>String</tt>
	 */
	public String getText();

}