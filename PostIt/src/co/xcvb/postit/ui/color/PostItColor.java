package co.xcvb.postit.ui.color;

import java.awt.Color;

/**
 * <p>
 * Colors for Post it note ui.
 * </p>
 * 
 * @author Calum Gardiner
 * @since 17/10/2014
 * @see Color
 * 
 */
public enum PostItColor {

	POSTIT_CANARY_YELLOW(new Color(247, 240, 141), "Canary Yellow"), POSTIT_TROPICAL_BLUE(new Color(86, 196, 232),
			"Tropical Blue"), POSTIT_LIGHT_PINK(new Color(205, 158, 192), "Light Pink");

	private String name;
	private Color color;
	public static final Color ALMOST_BLACK = new Color(0f, 0f, 0f, 0.25f);
	public static final Color ALMOST_TRANSPARENT = new Color(0f, 0f, 0f, 0.05f);
	public static final Color TRANSPARENT = new Color(0f, 0f, 0f, 0f);

	PostItColor(Color color, String name) {
		this.name = name;
		this.color = color;
	}

	/**
	 * Get the actual <tt>Color</tt>
	 * 
	 * @return color
	 */
	public Color getColor() {
		return this.color;
	}

	/**
	 * Get the PostItColor's name.
	 * 
	 * @return <tt>String</tt>
	 */
	public String getName() {
		return this.name;
	}
	
	public static PostItColor getColorFromString(String name) {
		for (PostItColor color : PostItColor.values()) {
			if (color.getName().equals(name)) {
				return color;
			}
		}
		return null;
	}

}
