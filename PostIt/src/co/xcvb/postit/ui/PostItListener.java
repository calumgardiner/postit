package co.xcvb.postit.ui;

import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.BadLocationException;

import co.xcvb.postit.data.PostIt;

/**
 * <p>
 * Post it listener implementation of DocumentListener. Uses dumb, full replace
 * on change, behaviour... for now.
 * </p>
 * 
 * @author Calum Gardiner
 * @since 17/10/2014
 * @see DocumentListener
 * 
 */
public class PostItListener implements DocumentListener {

	private PostIt postit;

	public PostItListener(PostIt postit) {
		this.postit = postit;
	}

	@Override
	public void insertUpdate(DocumentEvent e) {
		try {
			String text = e.getDocument().getText(0, e.getDocument().getLength());
			postit.setText(text);
		} catch (BadLocationException e1) {
			e1.printStackTrace();
		}
	}

	@Override
	public void removeUpdate(DocumentEvent e) {
		try {
			String text = e.getDocument().getText(0, e.getDocument().getLength());
			postit.setText(text);
		} catch (BadLocationException e1) {
			e1.printStackTrace();
		}
	}

	@Override
	public void changedUpdate(DocumentEvent e) {
		try {
			String text = e.getDocument().getText(0, e.getDocument().getLength());
			postit.setText(text);
		} catch (BadLocationException e1) {
			e1.printStackTrace();
		}
	}

}
