package co.xcvb.postit.ui.factory;

import co.xcvb.postit.data.PostItData;
import co.xcvb.postit.ui.PostItFrame;
import co.xcvb.postit.ui.color.PostItColor;

/**
 * <p>
 * Factory for creating PostItFrames.
 * </p>
 * 
 * @author Calum Gardiner
 * @since 17/10/2014
 * 
 */
public class PostItFactory {

	public static PostItFrame getInstance() {
		return new PostItFrame(new PostItData());
	}

	public static PostItFrame getInstance(String text) {
		return new PostItFrame(new PostItData(text));
	}

	public static PostItFrame getInstance(int width, int height) {
		return new PostItFrame(new PostItData(), width, height);
	}

	public static PostItFrame getInstance(int width, int height, PostItColor color) {
		return new PostItFrame(new PostItData(), width, height, color);
	}

	public static PostItFrame getInstance(String text, int width, int height) {
		return new PostItFrame(new PostItData(text), width, height);
	}

	public static PostItFrame getInstance(String text, int width, int height, PostItColor color) {
		return new PostItFrame(new PostItData(text), width, height, color);
	}
	
	public static PostItFrame getInstance(String text, int width, int height, String color, int x, int y) {
		PostItColor actualColor = PostItColor.getColorFromString(color);
		PostItFrame postIt = new PostItFrame(new PostItData(text), width, height, actualColor);
		postIt.setLocation(x, y);
		return postIt;
	}
}
