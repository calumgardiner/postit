package co.xcvb.postit.ui;

import java.awt.BorderLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import co.xcvb.postit.ui.color.PostItColor;
import co.xcvb.postit.ui.icon.ResizerIcon;

/**
 * <p>
 * Post it resizer.
 * </p>
 * 
 * @author Calum Gardiner
 * @since 17/10/2014
 * @see JPanel
 * @see ResizerIcon
 * 
 */
public class PostItResizer extends JPanel {

	private static final long serialVersionUID = -8444963767575319161L;
	private JFrame parent;
	private JLabel draggerIcon;
	private int xOnScreen;
	private int yOnScreen;
	private int originalWidth;
	private int originalHeight;

	public PostItResizer(JFrame parent) {
		super();
		this.parent = parent;
		this.setupPanel();
		this.setupListener();
	}

	private void setupPanel() {
		this.setBackground(PostItColor.TRANSPARENT);
		draggerIcon = new JLabel(new ResizerIcon(10, 10));
		this.setLayout(new BorderLayout());
		this.add(draggerIcon, BorderLayout.LINE_END);
	}

	private void setupListener() {
		this.draggerIcon.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				xOnScreen = e.getXOnScreen();
				yOnScreen = e.getYOnScreen();
				originalWidth = parent.getWidth();
				originalHeight = parent.getHeight();
			}
		});
		this.draggerIcon.addMouseMotionListener(new MouseMotionAdapter() {
			@Override
			public void mouseDragged(MouseEvent e) {
				if (SwingUtilities.isLeftMouseButton(e)) {
					int xChange = e.getXOnScreen() - xOnScreen;
					int yChange = e.getYOnScreen() - yOnScreen;
					int newWidth = originalWidth + xChange;
					int newHeight = originalHeight + yChange;
					if (newWidth < 100)
						newWidth = 100;
					if (newHeight < 100)
						newHeight = 100;
					parent.setSize(newWidth, newHeight);
				}
			}
		});
	}
}
