package co.xcvb.postit.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingUtilities;

import co.xcvb.postit.ui.color.PostItColor;
import co.xcvb.postit.ui.icon.CloseIcon;
import co.xcvb.postit.ui.icon.ColorIcon;
import co.xcvb.postit.ui.icon.NewIcon;

/**
 * <p>
 * Dragger and menu for the undecorated post it note.
 * </p>
 * 
 * @author Calum Gardiner
 * @since 17/10/2014
 * @see JPanel
 * 
 */
public class PostItDraggerMenu extends JPanel {

	private static final long serialVersionUID = 3993654722793491215L;
	private JFrame parent;
	private int xOnFrame;
	private int yOnFrame;
	private JLabel closeIcon;
	private JPopupMenu popupColorSelector;
	private JLabel newIcon;

	public PostItDraggerMenu(JFrame parent) {
		super();
		this.parent = parent;
		this.setupPanel();
		this.setupListeners();
	}

	private void setupPanel() {
		this.setBackground(PostItColor.ALMOST_TRANSPARENT);
		this.closeIcon = new JLabel(new CloseIcon(20, 20, 5));
		this.setLayout(new BorderLayout(5, 5));
		this.add(closeIcon, BorderLayout.LINE_END);
		this.newIcon = new JLabel(new NewIcon(20, 20, 5));
		this.add(newIcon, BorderLayout.LINE_START);
	}

	private void setupListeners() {
		this.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				if (SwingUtilities.isLeftMouseButton(e)) {
					xOnFrame = e.getX();
					yOnFrame = e.getY();
				}
				if (SwingUtilities.isRightMouseButton(e)) {
					showMenu(e.getXOnScreen(), e.getYOnScreen());
				}
			}
		});
		this.addMouseMotionListener(new MouseMotionAdapter() {
			@Override
			public void mouseDragged(MouseEvent e) {
				if (SwingUtilities.isLeftMouseButton(e)) {
					parent.setLocation((e.getXOnScreen() - xOnFrame), (e.getYOnScreen() - yOnFrame));
				}
			}
		});
		this.closeIcon.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (parent instanceof PostItFrame) {
					((PostItFrame) parent).close();
				}
			}
		});
		this.newIcon.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (parent instanceof PostItFrame) {
					((PostItFrame) parent).newPostIt();
				}
			}
		});
	}

	private void showMenu(int x, int y) {
		if (popupColorSelector == null) {
			popupColorSelector = new JPopupMenu();
			for (PostItColor color : PostItColor.values()) {
				popupColorSelector.add(colorItem(color));
			}
		}
		popupColorSelector.setLocation(x, y);
		popupColorSelector.setVisible(true);
		popupColorSelector.setEnabled(true);
	}

	private JMenuItem colorItem(final PostItColor color) {
		final JMenuItem colorItem = new JMenuItem(color.getName(), new ColorIcon(10, 10, color.getColor()));
		colorItem.setRolloverEnabled(true);
		colorItem.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				popupColorSelector.setVisible(false);
				if (parent instanceof PostItFrame) {
					((PostItFrame) parent).setColor(color);
				}
				colorItem.setBackground(Color.WHITE);
			}

			@Override
			public void mouseEntered(MouseEvent e) {
				colorItem.setBackground(color.getColor());
			}

			@Override
			public void mouseExited(MouseEvent e) {
				colorItem.setBackground(Color.WHITE);
			}
		});
		return colorItem;
	}
}
