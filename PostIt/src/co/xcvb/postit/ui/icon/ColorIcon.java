package co.xcvb.postit.ui.icon;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;

import javax.swing.Icon;

/**
 * <p>
 * Color icon, variable size and color. Provides square of color with grey
 * border.
 * </p>
 * 
 * @author Calum Gardiner
 * @since 17/10/2014
 * @see Icon
 * 
 */
public class ColorIcon implements Icon {
	
	private int width;
	private int height;
	private Color color;

	/**
	 * Default constructor.
	 * 
	 * @param width
	 * @param height
	 * @param color
	 */
	public ColorIcon(int width, int height, Color color) {
		this.width = width;
		this.height = height;
		this.color = color;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.Icon#paintIcon(java.awt.Component, java.awt.Graphics,
	 * int, int)
	 */
	@Override
	public void paintIcon(Component c, Graphics g, int x, int y) {
		Graphics2D g2 = (Graphics2D) g;
		RenderingHints rh = new RenderingHints(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g2.setRenderingHints(rh);
		g2.setColor(color);
		g2.fillRect(x, y, getIconWidth(), getIconHeight());
		g2.setStroke(new BasicStroke(2f));
		g2.setColor(Color.GRAY);
		g2.drawRect(x, y, getIconWidth(), getIconHeight());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.Icon#getIconWidth()
	 */
	@Override
	public int getIconWidth() {
		return this.width;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.Icon#getIconHeight()
	 */
	@Override
	public int getIconHeight() {
		return this.height;
	}
}
