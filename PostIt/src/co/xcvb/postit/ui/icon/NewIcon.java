package co.xcvb.postit.ui.icon;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;

import javax.swing.Icon;

public class NewIcon implements Icon {

	private int width;
	private int height;
	private int inset;

	/**
	 * Default constructor.
	 * 
	 * @param width
	 * @param height
	 */
	public NewIcon(int width, int height, int inset) {
		this.width = width;
		this.height = height;
		this.inset = inset;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.Icon#paintIcon(java.awt.Component, java.awt.Graphics,
	 * int, int)
	 */
	@Override
	public void paintIcon(Component c, Graphics g, int x, int y) {
		Graphics2D g2 = (Graphics2D) g;
		RenderingHints rh = new RenderingHints(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g2.setRenderingHints(rh);
		g2.setStroke(new BasicStroke(3f));
		g2.setColor(Color.GRAY);
		g2.drawLine(0 + inset, getIconWidth() / 2, getIconHeight() - inset, getIconWidth() / 2);
		g2.drawLine(getIconHeight() / 2, 0 + inset, getIconHeight() / 2, getIconWidth() - inset);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.Icon#getIconWidth()
	 */
	@Override
	public int getIconWidth() {
		return this.width;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.Icon#getIconHeight()
	 */
	@Override
	public int getIconHeight() {
		return this.height;
	}

}
