package co.xcvb.postit.ui.icon;

import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RenderingHints;

import javax.swing.Icon;

import co.xcvb.postit.ui.color.PostItColor;

/**
 * <p>
 * Resizer Icon, variable size generated with {@link Graphics2D}. Simply 2 lines
 * in the corner of the icon.
 * </p>
 * 
 * @author Calum Gardiner
 * @since 17/10/2014
 * @see Icon
 * 
 */
public class ResizerIcon implements Icon {
	
	private int width;
	private int height;

	/**
	 * Default Constructor.
	 * 
	 * @param width
	 * @param height
	 */
	public ResizerIcon(int width, int height) {
		this.width = width;
		this.height = height;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.Icon#paintIcon(java.awt.Component, java.awt.Graphics,
	 * int, int)
	 */
	@Override
	public void paintIcon(Component c, Graphics g, int x, int y) {
		Graphics2D g2 = (Graphics2D) g;
		RenderingHints rh = new RenderingHints(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g2.setRenderingHints(rh);
		g2.setColor(PostItColor.ALMOST_BLACK);
		Point topRight = new Point(getIconWidth(), 0);
		Point bottomLeft = new Point(0, getIconHeight());
		g2.drawLine(bottomLeft.x, bottomLeft.y, topRight.x, topRight.y);
		g2.drawLine(bottomLeft.x, bottomLeft.y + (getIconWidth() / 2), topRight.x + (getIconHeight() / 2), topRight.y);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.Icon#getIconWidth()
	 */
	@Override
	public int getIconWidth() {
		return this.width;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.Icon#getIconHeight()
	 */
	@Override
	public int getIconHeight() {
		return this.height;
	}
}
