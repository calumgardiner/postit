package co.xcvb.postit.ui.icon;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RenderingHints;

import javax.swing.Icon;

/**
 * <p>
 * Close Icon, variable size generated with {@link Graphics2D}. 2 Crossing
 * lines.
 * </p>
 * 
 * @author Calum Gardiner
 * @since 17/10/2014
 * @see Icon
 * 
 */
public class CloseIcon implements Icon {

	private int width;
	private int height;
	private int inset;

	/**
	 * Default constructor.
	 * 
	 * @param width
	 * @param height
	 * @param inset
	 */
	public CloseIcon(int width, int height, int inset) {
		this.width = width;
		this.height = height;
		this.inset = inset;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.Icon#paintIcon(java.awt.Component, java.awt.Graphics,
	 * int, int)
	 */
	@Override
	public void paintIcon(Component c, Graphics g, int x, int y) {
		Graphics2D g2 = (Graphics2D) g;
		RenderingHints rh = new RenderingHints(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g2.setRenderingHints(rh);
		g2.setColor(Color.GRAY);
		Point topRight = new Point(getIconWidth() - inset, 0 + inset);
		Point bottomLeft = new Point(0 + inset, getIconHeight() - inset);
		Point topLeft = new Point(0 + inset, 0 + inset);
		Point bottomRight = new Point(getIconWidth() - inset, getIconHeight() - inset);
		g2.setStroke(new BasicStroke(3f));
		g2.drawLine(bottomLeft.x, bottomLeft.y, topRight.x, topRight.y);
		g2.drawLine(bottomRight.x, bottomRight.y, topLeft.x, topLeft.y);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.Icon#getIconWidth()
	 */
	@Override
	public int getIconWidth() {
		return this.width;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.Icon#getIconHeight()
	 */
	@Override
	public int getIconHeight() {
		return this.height;
	}
}
