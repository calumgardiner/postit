package co.xcvb.postit.ui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;

import co.xcvb.postit.data.PostIt;
import co.xcvb.postit.ui.color.PostItColor;

/**
 * <p>
 * Post it note ui.
 * </p>
 * 
 * @author Calum Gardiner
 * 
 */
public class PostItFrame extends JFrame {

	public static final String DELETE = "DELETE";
	public static final String NEW = "NEW";

	private static final long serialVersionUID = -7876135750689561683L;
	private PostIt postit;
	private JPanel mainPanel;
	private JTextArea textField;
	private PostItDraggerMenu dragger;
	private PostItResizer resizer;
	private int width;
	private int height;
	private PostItColor color = PostItColor.POSTIT_CANARY_YELLOW;

	private ActionListener listener;

	/**
	 * Default Constructor. Provides default size and default color.
	 * 
	 * @param postit
	 */
	public PostItFrame(PostIt postit) {
		super();
		this.postit = postit;
		this.width = 300;
		this.height = 300;
		buildUI();
	}

	/**
	 * Constructor with size parameters.
	 * 
	 * @param postit
	 * @param width
	 * @param height
	 */
	public PostItFrame(PostIt postit, int width, int height) {
		super();
		this.postit = postit;
		this.width = width;
		this.height = height;
		buildUI();
	}

	/**
	 * Constructor with size and color parameters.
	 * 
	 * @param postit
	 * @param width
	 * @param height
	 * @param color
	 */
	public PostItFrame(PostIt postit, int width, int height, PostItColor color) {
		super();
		this.postit = postit;
		this.width = width;
		this.height = height;
		this.color = color;
		buildUI();
	}

	private void buildUI() {
		this.mainPanel = new JPanel(new BorderLayout(5, 5));
		this.textField = new JTextArea(postit.getText());
		this.dragger = new PostItDraggerMenu(this);
		this.resizer = new PostItResizer(this);
		this.mainPanel.add(dragger, BorderLayout.PAGE_START);
		this.mainPanel.add(textField, BorderLayout.CENTER);
		this.mainPanel.add(resizer, BorderLayout.PAGE_END);
		this.getContentPane().setLayout(new BorderLayout());
		this.getContentPane().add(mainPanel, BorderLayout.CENTER);
		this.textField.setWrapStyleWord(true);
		this.textField.setLineWrap(true);
		this.textField.setMargin(new Insets(10, 10, 10, 10));
		Font font = new Font("Comic Sans MS", Font.PLAIN, 15);
		textField.setFont(font);
		listenToTextField(textField);
		disableKey('	');
		this.setUndecorated(true);
		this.setColor(color);
		this.getContentPane().setBackground(PostItColor.TRANSPARENT);
		this.setBackground(PostItColor.TRANSPARENT);
		this.pack();
		this.setResizable(true);
		this.setSize(width, height);
		Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
		this.setLocation((d.width / 2) - width / 2, (d.height / 2) - height / 2);
	}

	/**
	 * Set the color of this postit frame.s
	 * 
	 * @param color
	 */
	public void setColor(PostItColor color) {
		this.color = color;
		this.textField.setBackground(color.getColor());
		this.mainPanel.setBackground(color.getColor());
		this.repaint();
	}

	/**
	 * Close the postit, fire action asking the listener to delete this post it.
	 * 
	 */
	public void close() {
		ActionEvent e = new ActionEvent(this, 1, DELETE);
		this.listener.actionPerformed(e);
	}

	/**
	 * New post it, fire actino asking the listener to create a new postit.
	 */
	public void newPostIt() {
		ActionEvent e = new ActionEvent(this, 1, NEW);
		this.listener.actionPerformed(e);
	}

	private void listenToTextField(JTextArea textField) {
		textField.getDocument().addDocumentListener(new PostItListener(postit));
	}

	public String getText() {
		return this.postit.getText();
	}

	public int getWidth() {
		return this.width;
	}

	public int getHeight() {
		return this.height;
	}

	public PostItColor getColor() {
		return this.color;
	}

	public void setListener(ActionListener listener) {
		this.listener = listener;
	}

	public void disableKey(final char c) {
		this.textField.addKeyListener(new KeyListener() {
			@Override
			public void keyTyped(KeyEvent e) {
				if (e.getKeyChar() == c) {
					e.consume();
				}
			}
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyChar() == c) {
					e.consume();
				}
			}
			@Override
			public void keyReleased(KeyEvent e) {
				if (e.getKeyChar() == c) {
					e.consume();
				}
			}

		});
	}

}
