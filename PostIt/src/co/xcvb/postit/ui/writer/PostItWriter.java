package co.xcvb.postit.ui.writer;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import co.xcvb.postit.ui.PostItFrame;

/**
 * <p>
 * Post it writer, writes post its to a serialized file.
 * </p>
 * 
 * @author Calum Gardiner
 * @since 17/10/2014
 */
public class PostItWriter {

	private String file;
	public static final String TOKEN = "\t";

	public PostItWriter(String file) {
		this.file = file;
	}

	/**
	 * Writes the given list of Post its to file.
	 * 
	 * @param postIts
	 * @throws IOException
	 */
	public void writePostIts(List<PostItFrame> postIts) throws IOException {
		FileOutputStream fout = new FileOutputStream(file);
		BufferedOutputStream bos = new BufferedOutputStream(fout);
		for (PostItFrame postIt : postIts) {
			bos.write(("" + postIt.getText() + TOKEN).getBytes());
			bos.write(("" + postIt.getWidth() + TOKEN).getBytes());
			bos.write(("" + postIt.getHeight() + TOKEN).getBytes());
			bos.write(("" + postIt.getColor().getName() + TOKEN).getBytes());
			bos.write(("" + postIt.getLocation().x + TOKEN).getBytes());
			bos.write(("" + postIt.getLocation().y).getBytes());
			if (!postIts.get(postIts.size() - 1).equals(postIt)) {
				bos.write((TOKEN + "\n").getBytes());
			}
		}

		bos.close();
	}
}
