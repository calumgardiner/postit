package co.xcvb.postit.ui.reader;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import co.xcvb.postit.ui.PostItFrame;
import co.xcvb.postit.ui.factory.PostItFactory;
import co.xcvb.postit.ui.writer.PostItWriter;

/**
 * <p>
 * Post it reader, reads in post its from a serialized file.
 * </p>
 * 
 * @author Calum Gardiner
 * @since 17/10/2014
 * 
 */
public class PostItReader {

	private String file;

	public PostItReader(String file) {
		this.file = file;
	}

	/**
	 * Read Post its from the file given.
	 * 
	 * @return <tt>List</tt><<tt>PostItsFrame</tt>>
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	public List<PostItFrame> readPostIts() throws IOException {
		File fileFile = new File(file);
		if (!fileFile.exists()) {
			fileFile.createNewFile();
		}
		BufferedReader br = new BufferedReader(new FileReader(file));
		List<PostItFrame> postIts = new ArrayList<PostItFrame>();
		try {
			StringBuilder sb = new StringBuilder();
			String line = br.readLine();

			while (line != null) {
				sb.append(line);
				sb.append(System.lineSeparator());
				line = br.readLine();
			}
			String everything = sb.toString();
			String[] everythingTokenized = everything.split(PostItWriter.TOKEN);
			if (everythingTokenized != null && everythingTokenized.length >= 6) {
				for (int i = 0; i < everythingTokenized.length;) {
					String text = everythingTokenized[i].trim();
					int width = Integer.parseInt(everythingTokenized[i + 1].trim());
					int height = Integer.parseInt(everythingTokenized[i + 2].trim());
					String color = everythingTokenized[i + 3].trim();
					int x = Integer.parseInt(everythingTokenized[i + 4].trim());
					int y = Integer.parseInt(everythingTokenized[i + 5].trim());
					postIts.add(PostItFactory.getInstance(text, width, height, color, x, y));
					i += 6;
				}
			} else {
				postIts.add(PostItFactory.getInstance());
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} finally {
			br.close();
		}
		return postIts;
	}
}
